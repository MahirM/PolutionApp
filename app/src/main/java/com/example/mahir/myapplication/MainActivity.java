package com.example.mahir.myapplication;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Random;

import com.google.android.gms.location.DetectedActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    BroadcastReceiver broadcastReceiver;

    private TextView txtActivity, txtConfidence;
    private ImageView imageActivity;
    private Button btnStartTrcking, btnStopTracking, torch;

    private RelativeLayout progressBar;
    private TextView textView, text, funnyText;
    private ImageView imageView;

 //   private Camera camera;
    private boolean isFlashOn;
    private boolean hasFlash;
    private Camera photo;
    private Camera.Parameters params;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.text);
        textView = findViewById(R.id.pollution_value);
        funnyText = findViewById(R.id.funny_text);
        imageView = findViewById(R.id.pollution);
        Button button = findViewById(R.id.breathin);
        torch = findViewById(R.id.camera);
        imageActivity = findViewById(R.id.image_activity);
        progressBar = findViewById(R.id.progress_bar);

        final String myNames[] = {"Sjeti se kako je bilo u ratu.", "Haj bar je dobro grijanje.",
                "Bar ne radiš ko skladištar u Bingu.", "Diši na auspuh.", "Uživajte u zračnoj banji Bukinje!",
                "But did you die!?", "Saće ba proljeće, za po godine!",
                "Ima sve gore od goreg!", "Nemoj sjedit na hladnom betonu!",
        };
        final String myNumbers[] = {"138", "178", "234", "213", "189", "129", "235", "245",
                "127", "146", "157", "134", "179", "200", "114", "147"};

        imageActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTracking();
            }
        });

        torch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(MainActivity.this,
                //       "on click button", Toast.LENGTH_SHORT).show();
                Intent in = new Intent(MainActivity.this, Main2Activity.class);
                in.putExtra("test", "testiranje");
                startActivity(in);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int r = (int) (Math.random() * 9);
                int e = (int) (Math.random() * 16);
                funnyText.setText(myNames[r]);
                funnyText.setTextSize(20);
                startTracking();
                //  textView.setText(myNumbers[e]);
                refreshData();

            }
        });

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Constants.BROADCAST_DETECTED_ACTIVITY)) {
                    int type = intent.getIntExtra("type", -1);
                    int confidence = intent.getIntExtra("confidence", 0);
                    handleUserActivity(type, confidence);
                }
            }
        };
        startTracking();
        refreshData();


    }

    private void refreshData() {
        showProgressBar();
        PolutionHelper.getPolutions(new PolutionHelper.ResponseHandler() {
            @Override
            public void onSuccess(List<Polution> polutions) {
                Polution polution = polutions.get(0);
                if (Integer.valueOf(polution.getAqi()) < 40) {
                    imageView.setImageResource(R.mipmap.happy);
                } else if (Integer.valueOf(polution.getAqi()) > 40 && Integer.valueOf(polution.getAqi()) < 120) {
                    imageView.setImageResource(R.mipmap.neutral);
                } else if (Integer.valueOf(polution.getAqi()) > 120 && Integer.valueOf(polution.getAqi()) < 180) {
                    imageView.setImageResource(R.mipmap.sad);
                } else if (Integer.valueOf(polution.getAqi()) > 180 && Integer.valueOf(polution.getAqi()) < 280){
                    imageView.setImageResource(R.mipmap.angry);
                } else if (Integer.valueOf(polution.getAqi()) > 280 && Integer.valueOf(polution.getAqi()) < 350) {
                    imageView.setImageResource(R.mipmap.dizzy);
                } else if (Integer.valueOf(polution.getAqi()) > 350) {
                    imageView.setImageResource(R.mipmap.skull);
                }
                if (polution.getCity().equals("Tuzlanski bukinje, Bosnia Herzegovina")) {
                    text.setText("Bukinje, Tuzla");
                } else if (polution.getCity().equals("Tuzlanski bkc, Bosnia Herzegovina")) {
                    text.setText("BKC, Tuzla");
                } else if (polution.getCity().equals("Tuzlanski skver, Bosnia Herzegovina")) {
                    text.setText("Skver, Tuzla");
                } else if (polution.getCity().equals("Tuzlanski lukavac, Bosnia Herzegovina")) {
                    text.setText("Lukavac");
                }
                textView.setText(polution.getAqi());
                hideProgressBar();
            }

            @Override
            public void onFailure() {
                hideProgressBar();
            }
        });
    }


    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 100);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void handleUserActivity(int type, int confidence) {
        int icon = R.mipmap.flag;

        switch (type) {
            case DetectedActivity.IN_VEHICLE: {
                //  label = getString(R.string.activity_in_vehicle);
                icon = R.mipmap.car;
                break;
            }
            case DetectedActivity.ON_BICYCLE: {
                //  label = getString(R.string.activity_on_bicycle);
                icon = R.mipmap.bicycle;
                break;
            }
            case DetectedActivity.ON_FOOT: {
                //  label = getString(R.string.activity_on_foot);
                icon = R.mipmap.walk;
                break;
            }
            case DetectedActivity.RUNNING: {
                // label = getString(R.string.activity_running);
                icon = R.mipmap.runner;
                break;
            }
            case DetectedActivity.STILL: {
                icon = R.mipmap.flag;
                //  label = getString(R.string.activity_still);
                break;
            }
//            case DetectedActivity.TILTING: {
//             //   label = getString(R.string.activity_tilting);
//                icon = R.drawable.ic_tilting;
//                break;
//            }
            case DetectedActivity.WALKING: {
                //  label = getString(R.string.activity_walking);
                icon = R.mipmap.walk;
                break;
            }
            case DetectedActivity.UNKNOWN: {
                icon = R.mipmap.flag;
                break;
            }
        }

        Log.e(TAG, "User activity: " + ", Confidence: " + confidence);

        if (confidence > Constants.CONFIDENCE) {
            //txtActivity.setText(label);
//            txtConfidence.setText("Confidence: " + confidence);
            imageActivity.setImageResource(icon);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(Constants.BROADCAST_DETECTED_ACTIVITY));
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    private void startTracking() {
        Intent intent1 = new Intent(MainActivity.this, BackgroundDetectedActivitiesService.class);
        startService(intent1);
    }

    private void stopTracking() {
        Intent intent = new Intent(MainActivity.this, BackgroundDetectedActivitiesService.class);
        stopService(intent);
    }
    private boolean isFlashSupported() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }
    private void showNoFlashAlert() {
     Toast.makeText(MainActivity.this, "No Flash!", Toast.LENGTH_LONG).show();

    }
}


