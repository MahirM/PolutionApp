package com.example.mahir.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by mahir on 1/20/18.
 */

public interface ApiResource {
    @GET("?bounds=44.494301281373104,18.50698471069336,44.570170566256174,18.70096206665039&inc=placeholders&k=_2Y2EzVh9IO1gdMzdNSzJWXmldXkc+AyNXFUY7IA==&_=1516467181837")
    Call<List<Polution>> getAllPolutions();
}
