package com.example.mahir.myapplication;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mahir on 1/20/18.
 */

public class PolutionHelper {
    public static void getPolutions(final ResponseHandler handler){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.waqi.info/mapq/bounds/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiResource apiResource = retrofit.create(ApiResource.class);
        Call<List<Polution>> call = apiResource.getAllPolutions();
        call.enqueue(new Callback<List<Polution>>() {
            @Override
            public void onResponse(Call<List<Polution>> call, Response<List<Polution>> response) {
                if (response != null && response.code() == 200){
                   handler.onSuccess(response.body());
                }else {
                    handler.onFailure();
                }
            }

            @Override
            public void onFailure(Call<List<Polution>> call, Throwable t) {
                handler.onFailure();
            }
        });

    }
    public interface ResponseHandler{
        void onSuccess(List<Polution> polutions);
        void onFailure();
    }
}
