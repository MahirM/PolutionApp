package com.example.mahir.myapplication;

/**
 * Created by mahir on 1/21/18.
 */

public class Constants {

    public static final String BROADCAST_DETECTED_ACTIVITY = "activity_intent";

    static final long DETECTION_INTERVAL_IN_MILLISECONDS = 2 * 1000;

    public static final int CONFIDENCE = 70;

}

