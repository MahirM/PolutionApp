package com.example.mahir.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Polution {

    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("idx")
    @Expose
    private Integer idx;
    @SerializedName("stamp")
    @Expose
    private Integer stamp;
    @SerializedName("pol")
    @Expose
    private String pol;
    @SerializedName("x")
    @Expose
    private String x;
    @SerializedName("aqi")
    @Expose
    private String aqi;
    @SerializedName("tz")
    @Expose
    private String tz;
    @SerializedName("utime")
    @Expose
    private String utime;
    @SerializedName("img")
    @Expose
    private String img;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getStamp() {
        return stamp;
    }

    public void setStamp(Integer stamp) {
        this.stamp = stamp;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getAqi() {
        return aqi;
    }

    public void setAqi(String aqi) {
        this.aqi = aqi;
    }

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public String getUtime() {
        return utime;
    }

    public void setUtime(String utime) {
        this.utime = utime;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Polution{" +
                "lat=" + lat +
                ", lon=" + lon +
                ", city='" + city + '\'' +
                ", idx=" + idx +
                ", stamp=" + stamp +
                ", pol='" + pol + '\'' +
                ", x='" + x + '\'' +
                ", aqi='" + aqi + '\'' +
                ", tz='" + tz + '\'' +
                ", utime='" + utime + '\'' +
                ", img='" + img + '\'' +
                '}';
    }
}